*** Settings ***
Documentation    These are test cases related to the functionality of adding products to the cart and verifying the total sum. It covers scenarios such as launching the browser, navigating to specific pages, searching for products, adding them to the cart, and verifying the correctness of the total price calculation.
Library           SeleniumLibrary

*** Test Cases ***
Add Multiple Products to Cart and Verify Total
    [Documentation]    This test case verifies the functionality of adding multiple products to the cart and ensuring the total price calculation is correct.
    Launch Browser
    Navigate to Page    ${BASE_URL}
    Navigate to Page    ${PRODUCTS_PAGE}
    Verify Search Bar Is Visible
    Enter Text Into Search Bar    ${SEARCH_FIRST_PRODUCT}
    Click Search Button
    Wait For Search Results To Load
    Verify Search Result    ${SEARCH_FIRST_PRODUCT}
    Scroll To Element By ClassNAme    ${PRODUCTS_LIST}
    Add Product To Cart
    #add second product
    Enter Text Into Search Bar    ${SEARCH_SECOND_PRODUCT}
    Click Search Button
    Wait For Search Results To Load
    Verify Search Result    ${SEARCH_SECOND_PRODUCT}
    Scroll To Element By ClassNAme    ${PRODUCTS_LIST}
    Add Product To Cart
    Click Cart Button
    Verify Products In Cart
    Verify Total Sum Of Products Is Correct

*** Variables ***
${BASE_URL}         http://automationexercise.com
${PRODUCTS_PAGE}    ${BASE_URL}/products
${SEARCH_FIRST_PRODUCT}      men tshirt
${SEARCH_SECOND_PRODUCT}     jeans
${SECOND_PRODUCT}   /html/body/section[2]/div/div/div[2]/div/div[2]/div/div[1]
${PRODUCTS_LIST}    features_items
${FIRST_PRODUCT}    single-products
${CART_TOTAL_PRICE_CLASS}     cart_total_price
${CART_PRICE_XPATH}     ./td[@class='cart_price']/p
*** Keywords ***
Launch Browser
    [Documentation]    Opens the web browser and navigates to the base URL.
    # the functions bellow are custom function from selenium library
    Open Browser    ${BASE_URL}    chrome
    Maximize Browser Window

Navigate to Page
    [Arguments]    ${url}
    [Documentation]    Navigates to the specified URL.
    Go To    ${url}
    Wait Until Element Is Visible    //*[@id="header"]/div/div/div/div[2]/div/ul/li[2]/a    90s

Verify Search Bar Is Visible
    [Documentation]    Verifies that the search bar is visible on the page.
    # the function bellow is custom function from selenium library
    Element Should Be Visible    //*[@id="header"]/div/div/div/div[2]/div/ul/li[2]/a

Enter Text Into Search Bar
    [Arguments]    ${text}
    [Documentation]    Enters the given text into the search bar.
    Input Text    //*[@id="search_product"]    ${text}

Click Search Button
    [Documentation]    Clicks the search button to initiate the product search.
    Click Button    //*[@id="submit_search"]

Wait For Search Results To Load
    [Documentation]    Waits until the page refreshes by checking the document ready state.
    ${ready_state} =    Execute JavaScript    return document.readyState;
    Wait Until Keyword Succeeds    1s    30s    Execute JavaScript    return document.readyState == 'complete';

Verify Search Result
    [Arguments]    ${text}
    [Documentation]     Verifies that the search result contains the specified text.
    ${result_text}=    get value    //*[@id="search_product"]
    Log    Search result text: ${result_text}
    Log    Search result text 2: ${text}

    Should Contain    ${result_text}    ${text}

Scroll To Element By ClassNAme
    [Arguments]  ${className}
    Execute JavaScript  document.getElementsByClassName('${className}')[0].scrollIntoView();

Add Product To Cart
    [Documentation]        Add product to the shopping cart.
    Mouse Over   xpath=/html/body/section[2]/div/div/div[2]/div/div[2]/div/div[1]
    Sleep                  2
    Execute JavaScript    document.querySelector('.btn.btn-default.add-to-cart').click()
    sleep    2s
    click button    //*[@id="cartModal"]/div/div/div[3]/button

Click Cart Button
    [Documentation]    Navigates to the shopping cart page by clicking on the 'Cart' button.
    Click Link    //*[@id="header"]/div/div/div/div[2]/div/ul/li[3]/a

Verify Products In Cart
    [Documentation]    Verifies that the products are visible in the cart.
    Element Should Be Visible    //*[@id="product-2"]

Verify Total Sum Of Products Is Correct
    [Documentation]    Verifies that the total sum of the products in the cart is correct.
    ${total_price} =  Calculate Total Price
    should be equal as integers    ${total_price}    1199

Calculate Total Price
    ${total_price}         Set Variable    ${0}
    ${rows}                execute javascript    return document.getElementsByClassName('${CART_TOTAL_PRICE_CLASS}')
    FOR    ${row}    IN    @{rows}
        ${price_text}      Get Text        ${row}
        log    Search result text: ${price_text}

        ${price}           Evaluate        int(${price_text.replace('Rs. ', '')})
        ${total_price}     Evaluate        ${total_price} + ${price}
    END
        [Return]    ${total_price}
